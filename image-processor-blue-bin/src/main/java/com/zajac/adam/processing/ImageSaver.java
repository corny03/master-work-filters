package com.zajac.adam.processing;

import com.zajac.adam.data.ImageDatabaseDAO;
import com.zajac.adam.data.ImageEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Created by zajac on 03.10.2017.
 * Task executor, makes processes in background
 */
@Service
public class ImageSaver {
    private final ImageDatabaseDAO imageDatabaseDAO;
    private ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();

    public ImageSaver(ImageDatabaseDAO imageDatabaseDAO) {
        this.imageDatabaseDAO = imageDatabaseDAO;
        taskExecutor.setCorePoolSize(4);
        taskExecutor.initialize();
    }

    public void processAndSaveBinarizedDataAsync(ImageEntity imageEntity, byte[] bytes) {
        taskExecutor.execute(new ProcessingThread(imageEntity, bytes, imageDatabaseDAO));
    }
}
