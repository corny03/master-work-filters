package com.zajac.adam.service;

import com.zajac.adam.data.DataSource;
import com.zajac.adam.data.ImageDatabaseDAO;
import com.zajac.adam.data.ImageEntity;
import com.zajac.adam.processing.ImageSaver;
import com.zajac.adam.processing.Status;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * Created by zajac on 20.11.2017.
 * Service provide management of entities from database
 */
@Service
public class EntityService {
    private final DataSource dataSource;
    private final ImageDatabaseDAO imageDatabaseDAO;
    private ImageSaver imageSaver;
    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    public EntityService(DataSource dataSource, ImageDatabaseDAO imageDatabaseDAO) {
        this.dataSource = dataSource;
        this.imageDatabaseDAO = imageDatabaseDAO;
        this.imageSaver = new ImageSaver(imageDatabaseDAO);
    }

    /**
     * @param id id of wanted image
     * @return entity of image
     * @throws FeignException with status 404. Created by datasource when photo doesn't exist
     *                        in main data storage micro service
     */
    public ImageEntity getImageEntity(String id) {
        String thisServiceId = appName + '-' + id;
        ImageEntity imageEntity = imageDatabaseDAO.findById(thisServiceId);
        if (imageEntity == null)
            return downloadMissingEntityProcessAndSave(id);
        return imageEntity;
    }

    public ImageEntity uploadToDatabase(String id, String fileName, byte[] bytes) {
        ImageEntity imageEntity = prepareImageEntityWithoutBytes(id, fileName);
        imageEntity = imageDatabaseDAO.save(imageEntity);
        imageSaver.processAndSaveBinarizedDataAsync(imageEntity, bytes);
        return imageEntity;
    }

    /**
     * @throws EmptyResultDataAccessException when entity not exist
     */
    public void deleteImage(String id) {
        String thisServiceId = appName + '-' + id;
        imageDatabaseDAO.delete(thisServiceId);
    }

    public void deleteAllImagesWhatContainInId(String originalId) {
        imageDatabaseDAO.deleteAllByIdLike("%" + originalId);
    }

    //##############################################################################

    private ImageEntity prepareImageEntityWithoutBytes(String id, String fileName) {
        String descriptionImageEntity = "Binarized image";
        java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        String thisServiceId = appName + '-' + id;
        return new ImageEntity(thisServiceId,
                fileName,
                descriptionImageEntity,
                null,
                sqlDate,
                Status.PROCESSING.toString());
    }

    private ImageEntity downloadMissingEntityProcessAndSave(String idFile) {
        ResponseEntity<ByteArrayResource> responseEntity = dataSource.getImage(idFile);
        byte[] imageBytes = responseEntity.getBody().getByteArray();
        String contentDisposition = responseEntity.getHeaders().getFirst("content-disposition");
        String fileName = contentDisposition.split("filename=")[1].split("\"")[1];
        return uploadToDatabase(idFile, fileName, imageBytes);
    }
}
