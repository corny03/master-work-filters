package com.zajac.adam.processing;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by zajac on 18.08.2017.
 * Class with algorithm to color binarize
 */
@Service
public class BinarizationProcessor {

    static byte[] processImage(byte[] originalImage) {
        BufferedImage bufferedImage = createImageFromBytes(originalImage);

        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        BufferedImage processedImage = new BufferedImage(width, height, bufferedImage.getType());		
		try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int rgb = bufferedImage.getRGB(i, j);
                double times = (height / 2) - j;
                int red = (rgb >> 16) & 0xff;
                int green = (rgb >> 8) & 0xff;
                int blue = (rgb) & 0xff;

                red = randomize(red, times);
                green = randomize(green, times);
                blue = randomize(blue, times);

                rgb = (rgb & 0xff000000) | (red << 16) | (green << 8) | (blue);
                processedImage.setRGB(i, j, rgb);
            }
        }
        return createByteArrayFromBufferedImage(processedImage);
    }

    private static int randomize(int colorVal, double level) {
        double randomVal = ThreadLocalRandom.current().nextDouble(0, 0.3) * level;

        int value = (int) ((double) colorVal + randomVal);
        if (value < 0)
            value = 0;
        else if (value > 255)
            value = 255;
        return value;
    }

    private static byte[] createByteArrayFromBufferedImage(BufferedImage bufferedImage) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bytes = null;
        try {
            ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            bytes = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    private static BufferedImage createImageFromBytes(byte[] imageData) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageData);
        try {
            return ImageIO.read(byteArrayInputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
