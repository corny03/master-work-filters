package com.zajac.adam.controller;

/**
 * Created by zajac on 10.08.2017.
 * List of path used in controller
 */
class Routing {
    static final String IMAGE_URL = "/{id}";
    static final String UPLOAD_URL = "/upload";
    static final String DELETE_URL = "/{id}";
    static final String DELETE_FROM_ALL_FILTERS_URL = "/all/{id}";
}
