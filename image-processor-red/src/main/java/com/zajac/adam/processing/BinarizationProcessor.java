package com.zajac.adam.processing;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by zajac on 18.08.2017.
 * Class with algorithm to color binarize
 */
@Service
public class BinarizationProcessor {

    static byte[] binarizeImage(byte[] originalImage) {
        BufferedImage bufferedImage = createImageFromBytes(originalImage);
        int[] redGreenBlueThreshold = dynamicThreshold(bufferedImage);

        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        BufferedImage binarizedImage = new BufferedImage(width, height, bufferedImage.getType());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int rgb = bufferedImage.getRGB(i, j);
                int red = (rgb >> 16) & 0xff;

                red = getBinarizedColorVal(red, redGreenBlueThreshold[0]);
                int green  = 0;
                int blue = 0;

                rgb = (rgb & 0xff000000) | (red << 16) | (green << 8) | (blue);
                binarizedImage.setRGB(i, j, rgb);
            }
        }
        return createByteArrayFromBufferedImage(binarizedImage);
    }

    private static int getBinarizedColorVal(int colorVal, int threshold) {
        if (colorVal > threshold)
            return 255;
        else
            return 0;
    }

    private static int[] dynamicThreshold(BufferedImage bufferedImage) {
        int[] redGreenBlueThreshold = new int[3];
        long redSum = 0;
        long greenSum = 0;
        long blueSum = 0;

        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int rgb = bufferedImage.getRGB(i, j);

                int red = (rgb >> 16) & 0xff;
                int green = (rgb >> 8) & 0xff;
                int blue = (rgb) & 0xff;

                redSum += red;
                greenSum += green;
                blueSum += blue;
            }
        }
        redGreenBlueThreshold[0] = (int) (redSum / (long) (width * height));
        redGreenBlueThreshold[1] = (int) (greenSum / (long) (width * height));
        redGreenBlueThreshold[2] = (int) (blueSum / (long) (width * height));

        return redGreenBlueThreshold;
    }

    private static byte[] createByteArrayFromBufferedImage(BufferedImage bufferedImage) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bytes = null;
        try {
            ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            bytes = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    private static BufferedImage createImageFromBytes(byte[] imageData) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageData);
        try {
            return ImageIO.read(byteArrayInputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
