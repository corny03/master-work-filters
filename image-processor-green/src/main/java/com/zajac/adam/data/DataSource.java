package com.zajac.adam.data;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by zajac on 10.08.2017.
 * Feign Client to Data Source Service
 */
@FeignClient(name = "DATA-SOURCE")
public interface DataSource {
    @GetMapping("{id}")
    ResponseEntity<ByteArrayResource> getImage(@PathVariable("id") String id);
}
